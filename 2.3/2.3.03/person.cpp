#include "person.hpp"
#include <iostream>

using namespace std;

void person::print(void)
{	
	cout << "This is " << name << endl;
	cout << "age = " << age << endl;
	cout << "male = " << male << endl;
	cout << "private_test = " << private_test << endl;
	cout << "protected_test = " << protected_test << endl;
	cout << "----------------" << endl;
}

void person::age_write(int age)
{
	this->age = age; 
}

void person::name_write(string name)
{
	this->name = name; 
}

//写private成员变量
void person::private_write(int test_val)
{
	this->private_test = test_val;
}

//写protected成员变量
void person::protected_write(int test_val)
{
	this->protected_test = test_val;
}
   
person::person(/* args */)
{
}

person::~person()
{
}


