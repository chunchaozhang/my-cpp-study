#ifndef __MAN_HPP_
#define __MAN_HPP_

#include "person.hpp"

using namespace std;

class man:public person
{
private:
    /* data */
public:
    man(/* args */);
    ~man();

    void man_test(void);
    void man_print(void);
};






#endif