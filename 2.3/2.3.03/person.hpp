#ifndef __PERSON_H__
#define __PERSON_H__


#include <string>
using namespace std;

class person;

class person
{
private:
	/* data */
	int private_test;

public:
	string name;
	int age;
	bool male;


	void print(void);
	void age_write(int age);
	void name_write(string name);

	void private_write(int test_val);
	void protected_write(int test_val);

	person();
	~person();

protected:
	int protected_test;

};



#endif

 