
# |此程序主要验证类继承中 public  private protected 这三种属性的继承特点|  
==========================

# 1、public 继承
子类在继承父类各种属性成员后的属性变化：

## public 成员：
依然是public属性，可以访问

## private 成员：
不能直接外部访问（父类都不可以，子类就更不用说了），也不可以在子类方法中直接访问，
只可以通过父类原有的可以访问private属性成员的方法（public的方法）进行访问

## protected 成员：
不能直接外部访问（父类都不可以，子类就更不用说了），也不可以在子类方法中直接访问，
只可以通过父类原有的可以访问protected属性成员的方法（public的方法）进行访问


# 2、private 继承
子类在继承父类各种属性成员后的属性变化：

## public 成员：
依然是public属性

## private 成员：

## protected 成员：



# 3、protected 继承
子类在继承父类各种属性成员后的属性变化：

## public 成员：
依然是public属性

## private 成员：

## protected 成员：



