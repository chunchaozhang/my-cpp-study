#include "man.hpp"
#include <iostream>

using namespace std;

man::man(/* args */)
{
}

man::~man()
{

}

void man::man_print(void)
{
   cout << "man::man_print , name = " << name << endl;
}


void man::man_test(void) 
{
    //  private_test = 1;  //报错，不能访问
     private_write(2);//可以访问
    //  this->protected = 2;//报错，不能访问
     protected_write(2);//可以访问
}