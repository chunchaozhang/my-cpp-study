/*
 * @Author: zcc
 * @Date: 2021-08-08 15:41:58
 * @LastEditTime: 2021-08-08 16:32:59
 * @LastEditors: Please set LastEditors
 * @Description: 测试 private 继承的各种属性成员的特点
 * @FilePath: \CPP\2.3\2.3.04\main.cpp
 */

#include <iostream>
#include "person.hpp"
#include "man.hpp"

using namespace std;

int main(void)
{
#if 1
	person p1;   //直接定义一个对象，分配在栈上，程序结束会自动清除空间

	p1.name_write("zhang san");
	p1.age_write(26);

	/*基类 private 访问测试*/
//	p1.private_test = 1;  //报错，不允许访问
	p1.private_write(2);//通过，可以通过类中的方法间接访问

	/*基类 protected 访问测试*/
	
	//p1.protected_test = 6;//报错，不允许访问
	p1.protected_write(3);//通过，可以通过类中的方法间接访问

	//打印所有成员变量
	p1.print();

#endif

#if 1
	man m1;
	
	//m1.man_test();
	//  m1.name_write("Li si");
	// m1.age_write(25);
	// m1.age = 22;

	/*基类 private 访问测试*/
	//m1.private_test = 1;//报错，不允许访问
	// m1.private_write(4);//通过，可以通过继承父类中的方法间接访问

	/*基类 protected 访问测试*/
	//m1.protected_test = 6;//报错，不允许访问
	// m1.protected_write(5); //通过，可以通过继承父类中的方法间接访问

	//打印所有（继承基类的）成员变量	 	
	// m1.print();
	
#endif
	return 0;
}

