/*
 * @Author: your name
 * @Date: 2021-08-08 16:16:35
 * @LastEditTime: 2021-08-08 16:21:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \my-cpp-study\2.3\2.3.04\person.hpp
 */
#ifndef __PERSON_H__
#define __PERSON_H__


#include <string>
using namespace std;

class person;

class person
{
private:
	/* data */
	int private_test;

public:
	string name;
	int age;
	bool male;
	int public_test;


	void print(void);
	void age_write(int age);
	void name_write(string name);

	void public_write(int test_val);
	void private_write(int test_val);
	void protected_write(int test_val);

	person();
	~person();

protected:
	int protected_test;

};



#endif

 