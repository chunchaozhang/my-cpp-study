/*
 * @Author: your name
 * @Date: 2021-08-08 15:41:58
 * @LastEditTime: 2021-08-08 16:31:40
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \CPP\2.3\2.3.04\man.hpp
 */
#ifndef __MAN_HPP_
#define __MAN_HPP_

#include "person.hpp"

using namespace std;

class man:protected person
{
private:
    /* data */
public:
    man(/* args */);
    ~man();

    void man_test(void);
    void man_print(void);
};






#endif