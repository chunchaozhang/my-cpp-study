/*
 * @Author: zcc
 * @Date: 2021-08-08 15:41:58
 * @LastEditTime: 2021-08-08 16:36:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \CPP\2.3\2.3.04\man.cpp
 */
#include "man.hpp"
#include <iostream>

using namespace std;

man::man(/* args */)
{
}

man::~man()
{

}

void man::man_print(void)
{
   //  cout << "man::man_print , name = " << name << endl;
}


void man::man_test(void) 
{
   public_test = 1; //可以访问
   public_write(2); //可以访问
   // private_test = 1;  //报错，不能访问
   private_write(2);//可以访问
   // this->protected = 2;//报错，不能访问
   protected_write(2);//可以访问
   
   print();
}