
#include <iostream>


using namespace std;


//用于相同同功能，但是参数类型可能不同的情况，解决了需要写功能重复的重载类型
template <typename T>

T add(T a,T b)
{
	return a+b;
}
//test  测试键盘

int main(void)
{

	int a = 1;
	int b = 1;
	
	cout << "a+b = " << add(a,b) << endl;

	double c = 1.11;
	double d = 1.7;
	
	cout << "c+d = " << add(c,d) << endl;

	return 0;
}
























