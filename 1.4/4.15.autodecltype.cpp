
#include <iostream>
#include <typeinfo>


using namespace std;

int func(void)
{
	cout << "func" << endl;
	return 0;
}

typedef int pf(void);

int main(void)
{
	 int i = 0;
	 auto a = func;
	 // decltype(func) a ;

	 cout << " func = " << func << endl;
	 cout << " a = " << a << endl;
	 cout << "typeid(a) = " << typeid(a).name() << endl;

	return 0;
}
























