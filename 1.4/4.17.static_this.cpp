
#include <iostream>


using namespace std;

class A
{
public:
	 int i;
	void func(void);
};

void A::func(void)
{
	cout << "A::func , i = " << i << endl;
}


class B
{
public:
	int i;
	static int j;
	static void func(void);
	void func2(void);
};

void B::func(void)
{
	cout << "B::func  "  << endl;
}

void B::func2(void)
{
	cout << "B::func , i = " << this->i  << endl;
}

int main(void)
{
	A a;

	a.i = 1;
	a.func();

	B b;
	b.i = 2;
	//b.j = 3;
	B::func();
	b.func2();

	return 0;
}
























